# Alternative scenarios

In the **Glue code** of the application there support to define scenarios on a lower abstraction level. This can be 
useful when a team is more development minded and want to simulate the interaction with the system more clear.

## Add a test using alternative scenario

Take the following steps to add the alternative scenario:

- Open the **src/test/resources/features/triangle-calculation.feature** file with the editor of choice.
- Add the following block of code to the feature and save the file.
```
Scenario: A triangle with all equal sides alternative
  Given Input calculator is a available
  When I enter for side 1 value 3
  And I enter for side 2 value 3
  And I enter for side 3 value 3
  Then the triangle is qualified as 'equilateral'
```
- Now add, commit and push the code using the commands.
```bash
git add .
git commit -am "Added alternative scenarios"
git push
```