# Setup CI/CD pipeline on GitLab

This project comes with a simple setup where already CI/CD pipeline is defined. This means every change made to project 
that is pushed to the remote repository will trigger a execution of the pipeline.

For more information on configuring GitLab CI can be found [here](https://docs.gitlab.com/ee/ci/yaml/)

## Setup GitLab CI/CD variables

To connect to the actual application a variable must be set in the environment of the pipeline. Follow these steps for 
setting the variables:

1. Go to pipeline configuration page use this URL https://gitlab.com/**gitlab-account**/cucumber-workshop/settings/ci_cd 
2. Go to the secret variables and click on **Expand**, this will open the input menu. ![Secret variables](images/secrets.png)
3. Create a new secret variable called **TRIANGLE_URL**, with a value provided in the workshop. ![Secret input](images/secret-input.png)
4. Add the variable by **Add variables**

## Going to the Cucumber report

After the pipeline run the Cucumber report will be available this can reached by the following steps:

1. On the pipeline overview select the latest run of the pipeline. ![Pipeline latest run](images/pipeline-run.png)
2. When on the overview of the pipeline run, click on the **acc_functional_test**. ![Pipeline run](images/pipeline.png)
3. On the right side a artifact pane is shown click **Browse**. ![Artifact browse](images/artifact-browse.png)
4. Follow the path **target/cucumber-reports/cucumber-html-reports/**.
5. Click on the **overview-features.html** link and the Cucumber report will be shown.
