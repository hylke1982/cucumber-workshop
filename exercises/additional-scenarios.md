# Adding a new Cucumber test

In this exercise a new test are based on the existing test. These test will validate also the other happy cases of the 
application. The test application Cucumber is structured in the following manner:

- Features written in Gherkin are located in de **src/test/resources/features** directory
- The glue code is available in the **/src/test/java/nl/codecentric/workshop/cucumber/definition/** directory. This glue
 code will connect the features with glue code written in Java.
- The **src/main/java/nl/codecentric/workshop/cucumber/definition/rest** contains the code for interfacing with the 
application under test.
- By choosing a level of abstraction in the Gherkin/Cucumber/Interface code it will be easier to refactor these test.

## Add tests for scalene and isosceles triangles

Currently only one test is supplied for a triangle with 3 equal sides, but also tests for 2 equals sides and no equals 
side can be added.

- Open the **src/test/resources/features/triangle-calculation.feature** file with the editor of choice.
- Add the following block of code to the feature and save the file

```
Scenario: A triangle with two equal sides
    Given Input calculator is a available
    When 2 equals sides are inputted
    Then the triangle is qualified as 'isosceles'
    
Scenario: A triangle with no equal sides
    Given Input calculator is a available
    When 1 equals sides are inputted
    Then the triangle is qualified as 'scalene'
```

- Now add, commit and push the code using the commands.

```
git add .
git commit -am "Added additional tests"
git push
```

